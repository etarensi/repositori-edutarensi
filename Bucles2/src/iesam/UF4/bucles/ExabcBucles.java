package iesam.UF4.bucles;

public class ExabcBucles {
	int num, i, j;

	public ExabcBucles(int num) {
		this.num = num;
	}

	public void bucle1() {

		char[][] m = new char[3][5];

		for (i = 0; i < 3; i++) {
			System.out.print("\n");
			for (j = 0; j < 5; j++) {
				if ((i == 0 && j == 2) || (i == 1 && j == 1)
						|| (i == 1 && j == 2) || (i == 1 && j == 3)
						|| (i == 2 && j == 0) || (i == 2 && j == 1)
						|| (i == 2 && j == 2) || (i == 2 && j == 3)
						|| (i == 2 && j == 4)) {
					m[i][j] = '*';
				} else {
					m[i][j] = ' ';
				}
			}
		}

		for (i = 0; i < 3; i++) {
			for (j = 0; j < 5; j++) {
				System.out.print(m[i][j]);
				System.out.print(m[i][j]);
			}
			System.out.print("\n");
		}
	}

	public void bucle2() {

		int cont = 0;

		System.out.print("\nEls primers 5 nombres parells son: ");

		for (i = 0; i < 100; i++) {
			if (i % 2 == 0) {
				System.out.print(i + " ");
				cont++;
			}
			if (cont == 5) {
				break;
			}
		}
	}

	public void bucle3() {
		int resultat = 0, num2, cont = 0;

		num2 = num;

		if (num2 % 10 == 0) {
			cont++;
			num2 /= 10;
		}

		while (num2 % 10 > 0) {
			cont++;
			num2 /= 10;
			if (num2 % 10 == 0) {
				cont++;
				num2 /= 10;
			}

		}
		cont -= 1;

		num2 = num;

		if (num2 % 10 == 0) {
			num2 /= 10;
		}

		while (num2 % 10 > 0) {
			resultat += Math.pow((num2 % 10), cont);
			num2 /= 10;
			if (num2 % 10 == 0) {
				num2 /= 10;
			}
		}

		if (resultat == num) {
			System.out.println("\n\nEs un numero d'Armstrong\n");
		} else {
			System.out.println("No ho es\n");
		}
	}

	public void bucle4() {

		char[][] matriu = new char[7][7];

		for (i = 0; i < 7; i++) {
			for (j = 0; j < 7; j++) {
				if ((i == 0 && j == 3) || (i == 1 && j == 2)
						|| (i == 1 && j == 3) || (i == 1 && j == 4)
						|| (i == 2 && j == 1) || (i == 2 && j == 2)
						|| (i == 2 && j == 3) || (i == 2 && j == 4)
						|| (i == 2 && j == 5) || (i == 3 && j == 0)
						|| (i == 3 && j == 1) || (i == 3 && j == 2)
						|| (i == 3 && j == 3) || (i == 3 && j == 4)
						|| (i == 3 && j == 5) || (i == 3 && j == 6)
						|| (i == 4 && j == 1) || (i == 4 && j == 2)
						|| (i == 4 && j == 3) || (i == 4 && j == 4)
						|| (i == 4 && j == 5) || (i == 5 && j == 2)
						|| (i == 5 && j == 3) || (i == 5 && j == 4)
						|| (i == 6 && j == 3)) {
					matriu[i][j] = '*';
				} else {
					matriu[i][j] = ' ';
				}
			}
		}

		for (i = 0; i <= 6; i++) {
			for (j = 0; j <= 6; j++) {
				System.out.print(matriu[i][j]);
			}
			System.out.println("\n");
		}

	}

	public void bucle5() {

		char[][] matriu = new char[7][7];

		for (i = 0; i < 7; i++) {
			for (j = 0; j < 7; j++) {
				if ((i == 0 && j == 3) || (i == 1 && j == 2)
						|| (i == 1 && j == 4) || (i == 2 && j == 1)
						|| (i == 2 && j == 5) || (i == 3 && j == 0)
						|| (i == 3 && j == 6) || (i == 4 && j == 1)
						|| (i == 4 && j == 5) || (i == 5 && j == 2)
						|| (i == 5 && j == 4) || (i == 6 && j == 3)) {
					matriu[i][j] = '*';
				} else {
					matriu[i][j] = ' ';
				}
			}
		}

		for (i = 0; i < 7; i++) {
			for (j = 0; j < 7; j++) {
				System.out.print(matriu[i][j]);
			}
			System.out.print("\n");
		}

	}

}
