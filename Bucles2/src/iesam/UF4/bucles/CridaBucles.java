package iesam.UF4.bucles;

import java.util.Scanner;

public class CridaBucles {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int num;
		
		Scanner sc = new Scanner(System.in);

		System.out.print("\n\nIntrodueix un nombre: ");
		num = sc.nextInt();
		sc.close();
		
		ExabcBucles losBucles = new ExabcBucles(num);
		
		losBucles.bucle1();
		losBucles.bucle2();
		losBucles.bucle3();
		losBucles.bucle4();
		losBucles.bucle5();
	}

}
